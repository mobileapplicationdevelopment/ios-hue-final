//
//  HueTableViewCell.swift
//  HueApplication
//
//  Created by Luuk Ros on 02/11/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import UIKit

class HueTableViewCell : UITableViewCell {
    
    var hueId : String = ""
    
    let baseUrl : String = "http://192.168.1.179/api/"
    let username : String = "5cd96f4231c302f33edd51c3a23e597"
    
    let hue = Hue()
    
    // Outlets for the hueName and the active-state (on/off) of a Hue
    @IBOutlet weak var hueName: UILabel!
    @IBOutlet weak var hueSwitch: UISwitch!
    
    // Action for setting the active-state (on/off) of a Hue
    @IBAction func hueSwitch(_ sender: UISwitch) {
        
        if hueSwitch.isOn {
            hue.setActive(url : "\(baseUrl)\(username)/lights/\(hueId)/state/")
        } else {
            hue.setInActive(url : "\(baseUrl)\(username)/lights/\(hueId)/state/")
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
