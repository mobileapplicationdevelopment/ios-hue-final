//
//  Hue.swift
//  HueApplication
//
//  Created by Luuk Ros on 02/11/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import Foundation
import UIKit


class Hue {
    
    // Hue-light id & name
    var id : String?
    var name : String?
    
    // Hue-light state-params
    var active : Bool?
    var brightness : Int?
    var hue : Int?
    var saturation : Int?
    var effect : String?
    var alert : String?
    
    let alamofireManager = AlamofireManager()
    
    // Setting an individual Hue to active (on)
    func setActive(url : String) {
        let params = ["on" : true]
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
    // Setting an individual Hue to inActive (off)
    func setInActive(url : String) {
        let params = ["on" : false]
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
    // Setting the brightness of an individual Hue
    func setBrightness(url : String, brightness : Int) {
        let params = ["bri" : brightness]
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
    // Setting the hue of an individual Hue
    func setHue(url : String, hue : Int) {
        let params = ["hue" : hue]
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
    // Setting the saturation of an individual Hue
    func setSaturation(url : String, saturation : Int) {
        let params = ["sat" : saturation]
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
    // Setting the effect-state of an individual Hue
    func setEffect(url : String, active : Bool) {
        // Default params for when effect is active (on)
        var params = ["effect" : "colorloop"]
        
        // Params for when effect is inactive (off)
        if !active {
            params = ["effect" : "none"]
        }
        
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
    // Setting the alert-state of an individual Hue
    func setAlert(url : String) {
        let params = ["alert" : "lselect"]
        alamofireManager.doRequest(url: url, params: params, method: .put)
    }
    
}
