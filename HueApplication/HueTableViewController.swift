//
//  HueTableViewController.swift
//  HueApplication
//
//  Created by Luuk Ros on 02/11/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HueTableViewController : UITableViewController {
    
    var hueLights : [Hue] = []
    
    // URL for testing in LA134
    let url = "http://192.168.1.179/api/5cd96f4231c302f33edd51c3a23e597/lights/"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getHueLights()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hueLights.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let hue = hueLights[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "hueCell", for: indexPath) as! HueTableViewCell
        
        cell.hueName.text = hue.name!
        cell.hueSwitch.setOn(hue.active!, animated: false)
        
        cell.hueId = hue.id!
        
        return cell
        
    }
    
    // Preparing for segue: TableView ~> DetailView
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "hueDetailSegue" {
            if let destination = segue.destination as? HueDetailViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    let hueLight = hueLights[(indexPath as NSIndexPath).row]
                    destination.hueLight = hueLight
                    
                    print(destination)
                }
            }
        }
    }
    
    // Gets all the HueLights in the current network using Alamofire & SwiftyJSON
    func getHueLights() {
        
        Alamofire.request(url,
                          method: .get,
                          encoding: JSONEncoding.default).responseJSON { (responseData) -> Void in if ((responseData.result.value) != nil) {
                            
                            let results = JSON(responseData.result.value!)
                            
                            for(key, result) in results {
                                print(results)
                                // Creating a new hueLight-object for each hue in the results
                                let hueLight = Hue()
                                
                                // Setting the hueId as key
                                let id = String(key)
                                
                                // Parsing all the relevant state-params of an individual Hue
                                let name = result["name"].string
                                let active = result["state"]["on"].bool
                                let brightness = result["state"]["bri"].int
                                
                                if let hue = result["state"]["hue"].int {
                                    hueLight.hue = hue
                                }
                                if let saturation = result["state"]["sat"].int {
                                    hueLight.saturation = saturation
                                }
                                
                                if let effect = result["state"]["effect"].string {
                                    hueLight.effect = effect
                                }
                                
                                let alert = result["state"]["alert"].string
                                
                                // Adding the parsed state-params to the hueLight-object
                                hueLight.id = id!
                                hueLight.name = name!
                                hueLight.active = active!
                                hueLight.brightness = brightness!
                                hueLight.alert = alert!
                                
                                // Adding the hueLight-object to the hueLights-array
                                self.hueLights.append(hueLight)
                                
                                // Reloading the tableView, which uses the hueLights-array
                                self.tableView.reloadData()
                                
                            }
                            
                            }
        }
        
    }

}
