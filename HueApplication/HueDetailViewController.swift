//
//  HueDetailViewController.swift
//  HueApplication
//
//  Created by Luuk Ros on 02/11/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class HueDetailViewController: UIViewController {
    
    var hueLight : Hue?
    
    let baseUrl : String = "http://192.168.1.179/api/"
    let username : String = "5cd96f4231c302f33edd51c3a23e597"
    
    // Outlet & Action for the Brightness-Slider
    @IBOutlet weak var brightnessSlider: UISlider!
    @IBAction func brightnessSlider(_ sender: UISlider) {
        let brightness = Int(brightnessSlider.value)
        hueLight?.setBrightness(url: "\(baseUrl)\(username)/lights/\(hueLight!.id!)/state/", brightness : brightness)
        print(brightness)
    }
    
    // Outlet & Action for the Hue-Slider
    @IBOutlet weak var hueSlider: UISlider!
    @IBAction func hueSlider(_ sender: UISlider) {
        let hue = Int(hueSlider.value)
        hueLight?.setHue(url: "\(baseUrl)\(username)/lights/\(hueLight!.id!)/state/", hue : hue)
    }
    
    // Outlet & Action for the Saturation-Slider
    @IBOutlet weak var saturationSlider: UISlider!
    @IBAction func saturationSlider(_ sender: UISlider) {
        let saturation = Int(saturationSlider.value)
        hueLight?.setSaturation(url: "\(baseUrl)\(username)/lights/\(hueLight!.id!)/state/", saturation: saturation)
        
    }
    
    // Outlet & Action for the Effect-Switch
    @IBOutlet weak var effectSwitch: UISwitch!
    @IBAction func effectSwitch(_ sender: UISwitch) {
        if effectSwitch.isOn {
            hueLight?.setEffect(url: "\(baseUrl)\(username)/lights/\(hueLight!.id!)/state/", active : true)
        } else {
            hueLight?.setEffect(url: "\(baseUrl)\(username)/lights/\(hueLight!.id!)/state/", active : false)
        }
    }
    
    // Outlet & Action for the Alert-Button
    @IBOutlet weak var alertButton: UIButton!
    @IBAction func alertButton(_ sender: UIButton) {
        hueLight?.setAlert(url: "\(baseUrl)\(username)/lights/\(hueLight!.id!)/state/")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setting up the Sliders for brightness, hue & saturation
        brightnessSlider.setValue(Float((hueLight!.brightness)!), animated: true)
        hueSlider.setValue(Float((hueLight!.hue)!), animated: true)
        saturationSlider.setValue(Float((hueLight!.saturation)!), animated: true)
        
        // Setting up the Switch for effects
        effectSwitch.setOn(false, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
