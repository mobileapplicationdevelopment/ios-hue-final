//
//  AlamofireManager.swift
//  HueApplication
//
//  Created by Luuk Ros on 02/11/16.
//  Copyright © 2016 Luuk Ros. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class AlamofireManager {
    
    func doRequest(url : String, params : [String : Any], method : HTTPMethod) {
        
        // Performing an Alamofire-request
        Alamofire.request(url,
                          method: method,
                          parameters: params,
                          encoding: JSONEncoding.default).responseJSON { (responseData) -> Void in
                            if((responseData.result.value) != nil) {
                            
                            let results = JSON(responseData.result.value!)
                            print("Response \(results)")
                            
                            }
                            
                        }
        
    }
    
}
